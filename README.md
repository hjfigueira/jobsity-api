# Jobsity - API

Dependencies
- Docker
- docker-compose


Setting Up

```
cp .env.dev .env
docker-compose up
docker-compose exec api composer install
docker-compose exec api php artisan migrate
```

Tests
```
docker-compose exec api /app/vendor/bin/phpunit
```

