<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 *  Routes for the first version of the api
 */
$router->group(['prefix' => 'v1'], function() use ($router){

    /** Public routes, that dont require auth */
    $router->group(['prefix' => 'public'], function() use($router) {

        /** User Bindings */
        $router->post('/user/signup', 'AuthController@postSignup');
        $router->post('/user/login', 'AuthController@postLogin');

        /** Service Status */
        $router->get('/ping', function () use ($router) { return ['pong' => true]; });

    });

    /** Routes that the user must be logged in */
    $router->group([ 'middleware' => ['auth'], 'prefix' => 'private' ], function() use ($router){

        /** Transaction bindings */
        $router->post('/transaction/deposit', 'TransactionController@postDeposit');
        $router->post('/transaction/withdraw', 'TransactionController@postWithdraw');
        $router->get('/transaction/historic', 'TransactionController@getAll');

        /** Account bindings */
        $router->get('/account/balance', 'AccountController@getBalance');

        /** User bindings */
        $router->delete('/user/logoff', 'AuthController@deleteLogoff');

    });
});
