<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Classe to map an user in the api
 * @package App\Models
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    protected $fillable = [
        'name', 'email', 'currency_code',
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $hidden = [
        'password', 'api_token'
    ];

    /**
     * Relationship between user and transactions
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    /**
     * Method for creating a new user
     * @param array $values
     * @return User|null
     */
    public static function createNew(array $values) : ?User
    {
        $newUser = new User();
        $newUser->fill($values);

        $password = Arr::get($values, 'password');
        $newUser->password = Hash::make($password);

        if($newUser->save())
        {
            return $newUser;
        }

        return null;
    }

    /**
     * Method for generating and saving a new APi Key
     * @return string|null
     */
    public function generateApiToken() : ?string
    {

        $seed = time().$this->email.$this->id;
        $this->api_token = base64_encode(Hash::make($seed));
        $this->save();

        return $this->api_token;

    }

    /**
     * Method for erasing the API token. Terminating the user session
     */
    public function eraseApiToken() : void
    {
        $this->api_token = null;
        $this->save();
    }

    /**
     * Method for getting account balance
     * @todo If this was a larger project, this should be moved to an account controller.
     * @return int
     */
    public function getAccountBalance() : float
    {
        $lastValue = 0;
        $lastTransaction = $this->getLastTransaction();
        if($lastTransaction !== null){
            $lastValue = $lastTransaction->balance;
        }

        return $lastValue;
    }

    /**
     * Method to getting the last transaction
     * @todo Also, if this was a larger project, with many accounts per user, this would not belong here
     * @return Transaction
     */
    public function getLastTransaction() : ?Transaction
    {
        return $this->transactions()->orderBy('id','desc')->first();
    }
}
