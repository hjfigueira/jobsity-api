<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Exceptions\InvalidWithdraw;

/**
 * Model to Map transactions that the user make in hist "account"
 * Class Transaction
 * @package App\Models
 */
class Transaction extends Model
{
    const WITHDRAW  = 'widthdraw';
    const DEPOSIT   = 'deposit';

    public $timestamps = false;

    protected $dates = [
        'created_at',
    ];

    /**
     * Relationship between the transaction and the user
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Inserts a transaction as withdraw, removing values from the account
     * @param User $user
     * @param float $value
     * @param string|null $currencyCode
     * @return bool
     * @throws InvalidWithdraw
     */
    public static function insertWithdraw(User $user, float $value, string $currencyCode = null) : object
    {
        $lastValue = $user->getAccountBalance();

        if( ($lastValue - $value) < 0 ){
            throw new InvalidWithdraw('Your withdraw cannot be greater than your balance');
        }

        $transaction = new Transaction();
        $transaction->value = $value;
        $transaction->type = Transaction::WITHDRAW;
        $transaction->balance = $lastValue - $value;
        $transaction->created_at = new \DateTime();

        return $user->transactions()->save($transaction);
    }

    /**
     * Inserts a transaction as deposit, adding values to the account
     * @param User $user
     * @param float $value
     * @param string|null $currencyCode
     * @return bool
     */
    public static function insertDeposit(User $user, float $value, string $currencyCode = null) : object
    {
        $lastValue = $user->getAccountBalance();

        $transaction = new Transaction();
        $transaction->value = $value;
        $transaction->type = Transaction::DEPOSIT;
        $transaction->balance = $lastValue + $value;
        $transaction->created_at = new \DateTime();

        return $user->transactions()->save($transaction);
    }

}
