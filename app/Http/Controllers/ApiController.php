<?php

namespace App\Http\Controllers;

use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Base API Controller to take care of standardizing methods
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends BaseController
{
    /**
     * Standard way to send back a success response to the client
     * @param string $message
     * @param array $data
     * @return array
     */
    public function apiSuccess(string $message, array $data = [])
    {
        return [
            'error' => false,
            'validation' => null,
            'message' => $message,
            'data' => $data
        ];
    }

    /**
     * Standard way to send back an error response to the client
     * @param \Exception $exception
     * @param array $data
     * @return array
     */
    public function apiError(\Exception $exception, array $data = [])
    {
        $validation = [];

        if(env('APP_DEBUG')){
            $message = $exception->getMessage();
        }else{
            $message = 'An error occurred, please try again';
        }

        if($exception instanceof ValidationException){
            $validation = $exception->errors();
            $message    = $exception->getMessage();
        }elseif ($exception instanceof UnauthorizedException){
            $message    = $exception->getMessage();
        }

        return [
            'error' => true,
            'validation' => $validation,
            'message' => $message,
            'data' => $data
        ];
    }
}
