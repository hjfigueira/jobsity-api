<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * "Account" methods for the API, if the projects requested more accounts per user, this would have more code on it :)
 * Class AccountController
 * @package App\Http\Controllers
 */
class AccountController extends ApiController
{

    /**
     * Api Method to get the user balance
     * @url /account/balance
     * @param Request $request
     * @return array
     */
    public function getBalance(Request $request)
    {
        try
        {
            $user = $request->user();
            $balance = $user->getAccountBalance();

            return $this->apiSuccess('Query successful', [
                'balance'       => number_format($balance,2),
                'currency_code' => $user->currency_code,
                'date'          => new \DateTime()
            ]);

        }
        catch (\Exception $error)
        {
            return $this->apiError($error);
        }

    }

}
