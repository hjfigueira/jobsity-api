<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\UnauthorizedException;

/**
 * API methods for authentication
 * Class AuthController
 * @package App\Http\Controllers
 */
class AuthController extends ApiController
{
    /**
     * Method to create new users in the api
     * @url /user/signup
     * @param Request $request
     * @return array
     */
    public function postSignup(Request $request)
    {
        try
        {
            $this->validate($request,[
                'name'                  => 'required',
                'password'              => 'required',
                'password_confirmation' => 'required|same:password',
                'email'                 => 'required|email|unique:users',
                'currency_code'         => 'required|size:3|validCurrencyCode'
            ]);

            $bodyData = $request->json();
            $user = User::createNew($bodyData->all());
            $token = $user->generateApiToken();

            return $this->apiSuccess('User created successfully', [
                'token' => $token,
                'user' => $user->name
            ]);
        }
        catch (\Exception $error)
        {
            return $this->apiError($error);
        }
    }

    /**
     * Method to loggin in an user and retuning the token
     * @url /user/login
     * @param Request $request
     * @return array
     */
    public function postLogin(Request $request)
    {
        try
        {
            $this->validate($request,[
                'email'          => 'required',
                'password'      => 'required'
            ]);

            $bodyData = $request->json();

            $email = $bodyData->get('email');
            $pass = $bodyData->get('password');

            $user = User::where('email',$email)->first();
            if($user === null){
                throw new UnauthorizedException('Wrong credentials');
            }

            if(Hash::check($pass,$user->password)){
                $token = $user->generateApiToken();
            }else{
                throw new UnauthorizedException('Wrong credentials');
            }

            return $this->apiSuccess('Login successful',[
                'token' => $token,
                'user' => $user->name
            ]);
        }
        catch (\Exception $error)
        {
            return $this->apiError($error);
        }

    }

    /**
     * Method to terminating the user session
     * @url /user/logoff
     * @param Request $request
     * @return array
     */
    public function deleteLogoff(Request $request)
    {
        $user = $request->user();
        $user->eraseApiToken();

        return $this->apiSuccess('Logoff successful');
    }

}
