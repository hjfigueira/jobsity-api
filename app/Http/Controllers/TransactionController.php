<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Services\CurrencyServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Controller to map the restfull calls
 * Class TransactionController
 * @package App\Http\Controllers
 */
class TransactionController extends ApiController
{
    /** @var CurrencyServiceInterface */
    private CurrencyServiceInterface $currencyService;

    public function __construct(CurrencyServiceInterface $currencyService)
    {
        $this->currencyService = $currencyService;
    }


    /**
     * Inserts a withdraw into the transaction list
     * Method looks like duplicated, but this idea is to prepare for future differences. Too much reduction in this method
     * could lead to problems
     * @param Request $request
     * @return array
     */
    public function postWithdraw(Request $request)
    {
        try{

            $this->validate($request,[
                'value'                 => 'required|gt:0',
                'currency_code'         => 'size:3|validCurrencyCode'
            ]);

            $user = $request->user();
            $bodyData = $request->json();

            $value          = $bodyData->get('value');
            $currencyCode   = $bodyData->get('currency_code', null);

            if($currencyCode != null && ( $user->currency_code != $currencyCode ) ){
                $value = $this->currencyService->exchange($currencyCode, $user->currency_code, $value);
            }

            Transaction::insertWithdraw($user, (float) $value);

            return $this->apiSuccess('Withdraw successful');
        }
        catch (\Exception $error)
        {
            return $this->apiError($error);
        }
    }

    /**
     * Inserts a deposit into the account list
     * Method looks like duplicated, but this idea is to prepare for future differences. Too much reduction in this method
     * could lead to problems
     * @param Request $request
     * @return array
     */
    public function postDeposit(Request $request)
    {
        try
        {
            $this->validate($request,[
                'value'                 => 'required|gt:0',
                'currency_code'         => 'size:3|validCurrencyCode'
            ]);

            $user = $request->user();
            $bodyData = $request->json();

            $value          = $bodyData->get('value');
            $currencyCode   = $bodyData->get('currency_code', null);

            if($currencyCode != null && ( $user->currency_code != $currencyCode ) ){
                $value = $this->currencyService->exchange($currencyCode, $user->currency_code, $value);
            }

            Transaction::insertDeposit($user, (float) $value);

            return $this->apiSuccess('Deposit successful');
        }
        catch (\Exception $error)
        {
            return $this->apiError($error);
        }
    }

}
