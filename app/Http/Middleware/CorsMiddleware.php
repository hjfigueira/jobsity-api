<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Middleware to handle cors validation
 * Class CorsMiddleware
 * @package App\Http\Middleware
 */
class CorsMiddleware
{
    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $headers = [];

        $allowedHosts = explode(',', env('ALLOWED_DOMAINS'));
        $requestHost = $request->getScheme() . '://' .  $request->getHost();

        if(in_array($requestHost, $allowedHosts)){
            $headers = [
                'Access-Control-Allow-Origin'      => $requestHost,
                'Access-Control-Allow-Methods'     => 'POST, GET, OPTIONS, PUT, DELETE',
                'Access-Control-Allow-Credentials' => 'true',
                'Access-Control-Max-Age'           => '86400',
                'Access-Control-Allow-Headers'     => 'Content-Type, X-Requested-With, X-Auth-Token'
            ];
        }

        if ($request->isMethod('OPTIONS'))
        {
            return response()->json('{"method":"OPTIONS"}', 200, $headers);
        }

        $response = $next($request);
        foreach($headers as $key => $value)
        {
            $response->header($key, $value);
        }

        return $response;
    }
}
