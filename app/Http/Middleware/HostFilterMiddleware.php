<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

/**
 * Allow use of the webservice only for pre-approved hosts
 * Class HostFilterMiddleware
 * @package App\Http\Middleware
 */
class HostFilterMiddleware
{
    /**
     * Check if the domain in one of the pre-approved
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $allowedHosts = explode(',', env('ALLOWED_DOMAINS'));
        $requestHost = $request->getScheme() . '://' .  $request->getHost();

        if(!in_array($requestHost, $allowedHosts)){
            return response([
                'error' => true,
                'message' => 'Unauthorized Host'
            ], 401);
        }

        return $next($request);
    }
}
