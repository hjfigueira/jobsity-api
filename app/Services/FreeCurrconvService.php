<?php

namespace App\Services;

use App\Exceptions\ApiError;
use GuzzleHttp;

class FreeCurrconvService implements CurrencyServiceInterface
{
    protected string $apiKey = '';


    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }


    public function exchange(string $codeFrom, string $codeTo, int $value) : float {

        $token = $this->apiKey;
        $client = new GuzzleHttp\Client();

        try {

            $valuePair = "{$codeFrom}_{$codeTo}";

            $response = $client->request(
                'GET',
                "https://free.currconv.com/api/v7/convert?q={$valuePair}&compact=ultra&apiKey=$token");

            $body = $response->getBody();
            if($body)
            {
                $responseJson = json_decode($body);
                if(isset($responseJson->$valuePair)){
                    return $responseJson->$valuePair * $value;
                }else{
                    throw new ApiError('External API Error');
                }

            }

        } catch (\GuzzleHttp\Exception\GuzzleException $e)
        {
            throw new ApiError('External API Error');
        }

    }

}
