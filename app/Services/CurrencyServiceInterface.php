<?php

namespace App\Services;

interface CurrencyServiceInterface
{
    public function exchange(string $codeFrom, string $codeTo, int $valueCents) : float;
}
