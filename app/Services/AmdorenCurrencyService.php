<?php

namespace App\Services;

use GuzzleHttp;

/**
 * Class AmdorenCurrencyService
 * @package App\Services
 * @deprecated This service is not working. It was not fully testes because the api limit was reached in mid development
 */
class AmdorenCurrencyService implements CurrencyServiceInterface
{
    protected string $apiKey = '';


    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }


    public function exchange(string $codeFrom, string $codeTo, int $value) : float {

        $token = $this->apiKey;
        $client = new GuzzleHttp\Client();

        try {

            $response = $client->request(
                'GET',
                "https://www.amdoren.com/api/currency.php?api_key=$token&from=$codeFrom&to=$codeTo&amount=$value");

            $body = $response->getBody();
            if($body)
            {
                $responseJson = json_decode($body);
                return $responseJson->amount;
            }

        } catch (\GuzzleHttp\Exception\GuzzleException $e)
        {

        }

    }

}
