<?php

namespace App\Services;

use App\Exceptions\ApiError;
use GuzzleHttp;

class FixerCurrencyService implements CurrencyServiceInterface
{
    protected string $apiKey = '';


    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }


    public function exchange(string $codeFrom, string $codeTo, int $value) : float{

        $token = $this->apiKey;
        $client = new GuzzleHttp\Client();

        try {

            $response = $client->request(
                'GET',
                "http://data.fixer.io/api/latest?access_key=$token&base=$codeFrom&symbols=$codeTo");

            $body = $response->getBody();
            if($body)
            {
                $conversionFactor = json_decode($body)->rates->$codeTo;
                return $value * $conversionFactor;
            }

        } catch (\GuzzleHttp\Exception\GuzzleException $e)
        {
            throw new ApiError('External API Error');
        }

    }


}
