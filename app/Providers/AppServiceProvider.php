<?php

namespace App\Providers;

use App\Services\AmdorenCurrencyService;
use App\Services\CurrencyServiceInterface;
use App\Services\FixerCurrencyService;
use App\Services\FreeCurrconvService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Validator::extend('validCurrencyCode', function($attribute, $value, $parameters) {

            $validCurrencyArray = ['AED','AFN','ALL','AMD','ANG','AOA','ARS','AUD','AWG','AZN','BAM','BBD','BDT','BGN',
                'BHD','BIF','BMD','BND','BOB','BOV','BRL','BSD','BTN','BWP','BYR','BZD','CAD','CDF','CHE','CHF','CHW',
                'CLF','CLP','CNY','COP','COU','CRC','CUC','CUP','CVE','CZK','DJF','DKK','DOP','DZD','ECS','EGP','ERN',
                'ETB','EUR','FJD','FKP','GBP','GEL','GHS','GIP','GMD','GNF','GTQ','GYD','HKD','HNL','HRK','HTG','HUF',
                'IDR','ILS','IMP','INR','IQD','IRR','ISK','JMD','JOD','JPY','KES','KGS','KHR','KMF','KPW','KRW','KWD',
                'KYD','KZT','LAK','LBP','LKR','LRD','LSL','LTL','LVL','LYD','MAD','MDL','MGA','MKD','MMK','MNT','MOP',
                'MRO','MUR','MVR','MWK','MXN','MXV','MYR','MZN','NAD','NGN','NIO','NOK','NPR','NZD','OMR','PAB','PEN',
                'PGK','PHP','PKR','PLN','PYG','QAR','RON','RSD','RUB','RWF','SAR','SBD','SCR','SDG','SEK','SGD','SHP',
                'SLL','SOS','SRD','STN','SVC','SYP','SZL','THB','TJS','TMT','TND','TOP','TRY','TTD','TWD','TZS','UAH',
                'UGX','USD','USN','USS','UYI','UYU','UZS','VES','VND','VUV','WST','XAF','XAG','XAU','XBA','XBB','XBC',
                'XBD','XCD','XDR','XFU','XOF','XPD','XPF','XPT','XTS','XXX','YER','ZAR','ZMW','ZWL'];

            return in_array($value, $validCurrencyArray);
        }, 'Invalid currency ISO code' );
    }


    /**
     * Registeting the external API service to query different currencies
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CurrencyServiceInterface::class, function ($app) {

            if(env('EXCHANGE_API') === 'fixer'){
                return new FixerCurrencyService(env('API_FIXER'));
            }elseif(env('EXCHANGE_API') === 'freecurr'){
                return new FreeCurrconvService(env('API_FREECURR'));
            }

            return new AmdorenCurrencyService(env('API_AMDOREN'));
        });
    }

}
