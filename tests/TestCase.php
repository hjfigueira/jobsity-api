<?php

use Laravel\Lumen\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }


    protected function insertUser()
    {
        $user = \App\Models\User::createNew(
            [
                'name' => 'Username Lastname',
                'email' => 'fake@email.com',
                'password' => '1234567890',
                'currency_code' => 'EUR'
            ]
        );

        return [$user->generateApiToken(), $user];
    }
}
