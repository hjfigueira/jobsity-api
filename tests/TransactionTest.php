<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

/**
 * Class SuccessesTransactionTest
 * Testing main flow for the transactions
 */
class TransactionTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Method for testing deposits
     */
    public function testDeposit()
    {
        list($token,$user) = $this->insertUser();

        $this->json('POST', 'v1/private/transaction/deposit', [
            'value' => 100.00,
        ],[
            'X-Auth-Token' => $token
        ])->seeJson([
            'error' => false,
        ])->seeStatusCode(200);
    }

    /**
     * Method for testing withdraws
     */
    public function testWithdraw()
    {
        list($token,$user) = $this->insertUser();

        \App\Models\Transaction::insertDeposit($user, 120.00);

        $this->json('POST', 'v1/private/transaction/withdraw', [
            'value' => 120.00,
        ],[
            'X-Auth-Token' => $token
        ])->seeJson([
            'error' => false,
        ])->seeStatusCode(200);

        $this->seeInDatabase('transactions', ['balance' => '0']);
    }

}
