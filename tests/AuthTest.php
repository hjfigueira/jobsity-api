<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

/**
 * Class SuccessesAuthTest
 * All tests here should validate the main flow scenarios
 */
class AuthTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example
     * @return void
     */
    public function testPing()
    {
        $this->json('GET', 'v1/public/ping', [])
            ->seeJson([
                'pong' => true,
            ]);
    }

    /**
     * Testing the creation of a new user
     */
    public function testSignup()
    {
        $this->json('POST', 'v1/public/user/signup', [
            'name' => 'Username Lastname',
            'email' => 'fake@email.com',
            'password' => '1234567890',
            'password_confirmation' => '1234567890',
            'currency_code' => 'EUR'
        ])->seeJson([
            'error' => false,
        ])->seeJson([
            'user' => 'Username Lastname'
        ]);

        $this->seeInDatabase('users', ['email' => 'fake@email.com']);
    }

    /**
     * Testing the login
     */
    public function testLogin()
    {
        $this->insertUser();

        $this->json('POST', 'v1/public/user/login', [
            'email' => 'fake@email.com',
            'password' => '1234567890',
        ])->seeJson([
            'error' => false,
        ])->seeJson([
            'user' => 'Username Lastname'
        ]);
    }
}
