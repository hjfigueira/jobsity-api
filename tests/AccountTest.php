<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

/**
 * Class SuccessesTransactionTest
 * Testing main flow for the accounts
 */
class AccountTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Method for testing the curent balance
     */
    public function testBalance()
    {
        list($token,$user) = $this->insertUser();

        \App\Models\Transaction::insertDeposit($user, 120.00);

        $this->json('GET', 'v1/private/account/balance', [
            'email' => 'fake@email.com',
            'password' => '1234567890',
        ],[
            'X-Auth-Token' => $token
        ])->seeJson([
            'error' => false,
        ])->seeJson([
            'balance' => "120.00"
        ])->seeStatusCode(200);
    }
}
