<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->id();

            $table->string('email',  180)
                ->nullable(false)
                ->unique('idx_email');

            $table->string('password',  250)
                ->nullable(false);

            $table->string('api_token',  180)
                ->nullable(true);

            $table->string('name', 180)
                ->nullable(false);

            $table->string('currency_code', 3)
                ->nullable(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
