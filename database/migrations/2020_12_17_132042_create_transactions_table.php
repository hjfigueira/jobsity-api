<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {

            $table->id();

            $table->bigInteger('value')
                ->nullable(false);

            $table->unsignedDecimal('balance',15, 6)
                ->nullable(false)
                ->unsigned();

            $table->string('type', 50 )
                ->nullable(false);

            $table->timestamp('created_at')
                ->nullable(false);

            $table->bigInteger('user_id')
                ->unsigned()
                ->nullable(false);

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
